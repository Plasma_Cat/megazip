﻿using MegaZipV2.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;

namespace MegaZipV2
{
    static class Program
    {
        static void Main(string[] args)
        {
            var compressionMode = (args.Length > 0 && args[0].Equals("Decompress")) ?
                                                      CompressionMode.Decompress :
                                                      CompressionMode.Compress;

            var inputFileName = args.Length > 1 ? args[1] : @"1.dat";
            var outputFileName = args.Length > 2 ? args[2] : @"1.big";

            if (args.Length < 1)
                compressionMode = GetCompressionMode();

            if (args.Length < 2)
                inputFileName = GetInputPath(compressionMode == CompressionMode.Compress);

            if (args.Length < 3)
                outputFileName = GetOutputPath(compressionMode == CompressionMode.Compress);

            var inputQueue = new ConcurrentQueue<NumberedChunk>(10);
            var zippedQueue = new ConcurrentQueue<byte[]>(10);
            int buffersize = 1024 * 1024;

            bool isCompress = compressionMode == CompressionMode.Compress;

            // create zip thread
            var zipThreads = new Thread[Environment.ProcessorCount];

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                var zipThread = new Thread(new ParameterizedThreadStart(Zip));
                zipThread.Start(new Dictionary<string, object> {
                     { "compression", compressionMode },
                     { "inputQueue", inputQueue },
                     { "outputQueue", zippedQueue },
                     { "buffersize", buffersize }
                 });
                zipThread.Name = "Zip Thread " + i;

                zipThreads[i] = zipThread;
            }

            var wrtThread = new Thread(new ParameterizedThreadStart(isCompress ? (Action<object>)WriteCompress :
                                                                                 (Action<object>)WriteDecompress));
            wrtThread.Start(new Dictionary<string, object> {
                     { "filePath", outputFileName },
                     { "inputQueue", zippedQueue }
            });
            wrtThread.Name = "Write Thread";

            if (isCompress)
                ReadFileCompress(inputFileName, inputQueue, buffersize);
            else
                ReadFileDecompress(inputFileName, inputQueue);

            inputQueue.Finish();

            foreach (var zipThread in zipThreads)
                zipThread.Join();

            zippedQueue.Finish();

            Console.WriteLine("Joined");
            Console.ReadKey();
        }

        private static string GetOutputPath(bool compressionMode)
        {
            string defaultOutputPath = compressionMode ? @"1.big" : @"2.dat";
            Console.WriteLine("Result file path? (by default \"{0}\"): ", defaultOutputPath);
            string tmp = Console.ReadLine().ToString();

            if (!string.IsNullOrEmpty(tmp))
                return tmp;
            else
                return defaultOutputPath;
        }

        private static string GetInputPath(bool compressionMode)
        {
            string defaultInputPath = compressionMode ? @"1.dat" : @"1.big";

            Console.WriteLine("Source file path? (by default \"{0}\"): ", defaultInputPath);
            string tmp = Console.ReadLine().ToString();

            if (!string.IsNullOrEmpty(tmp))
                return tmp;
            else
                return defaultInputPath;
        }

        private static CompressionMode GetCompressionMode()
        {
            Console.WriteLine("Comress or Decompress? [c|d] (default c): ");
            if (string.Compare(Console.ReadLine().ToString(), "d", ignoreCase: true) == 0)
            {
                return CompressionMode.Decompress;
            }
            else
                return CompressionMode.Compress;
        }

        private static void ReadFileCompress(string filePath, ConcurrentQueue<NumberedChunk> outputQueue, int chunksize)
        {
            int chunknum = 0;
            using (var stream = File.OpenRead(filePath))
            {
                foreach (var dataChunk in ReadFileChunk(stream, chunksize))
                {
                    var nch = new NumberedChunk(chunknum, dataChunk);
                    outputQueue.Enqueue(nch);
                    chunknum++;
                }
            }
        }

        private static void ReadFileDecompress(string filePath, ConcurrentQueue<NumberedChunk> outputQueue)
        {
            int chunknum = 0;
            using (var stream = File.OpenRead(filePath))
            {
                while (true)
                {
                    int chunksize = ReadChunkSize(stream);
                    if (chunksize < 1)
                        break;

                    var en = ReadFileChunk(stream, chunksize).GetEnumerator();
                    if (en.MoveNext())
                    {
                        var nch = new NumberedChunk(chunknum, en.Current);
                        outputQueue.Enqueue(nch);
                        chunknum++;
                    }
                    else
                        break;
                }
            }
        }

        private static void WriteCompress(object argsDictionary)
        {
            var args = argsDictionary as Dictionary<string, object>;

            var filePath = args["filePath"] as string;
            var inputQueue = args["inputQueue"] as ConcurrentQueue<byte[]>;

            using (var stream = File.OpenWrite(filePath))
            {
                foreach (var bts in inputQueue)
                {
                    var prefixBytes = Encoding.ASCII.GetBytes(bts.Length.ToString() + '#');

                    stream.Write(prefixBytes, 0, prefixBytes.Length);
                    stream.Write(bts, 0, bts.Length);
                }
            }
        }

        private static void WriteDecompress(object argsDictionary)
        {
            var args = argsDictionary as Dictionary<string, object>;

            var filePath = args["filePath"] as string;
            var inputQueue = args["inputQueue"] as ConcurrentQueue<byte[]>;

            using (var stream = File.OpenWrite(filePath))
            {
                foreach (var bts in inputQueue)
                {
                    stream.Write(bts, 0, bts.Length);
                }
            }
        }

        static int prevchunk = -1;

        private static void Zip(object argsDictionary)
        {
            var args = argsDictionary as Dictionary<string, object>;

            var compressionMode = (CompressionMode)args["compression"];
            var inputQueue = args["inputQueue"] as ConcurrentQueue<NumberedChunk>;
            var outputQueue = args["outputQueue"] as ConcurrentQueue<byte[]>;
            var bfs = (int)args["buffersize"];

            foreach (var data in inputQueue)
            {
                byte[] output = null;
                if (compressionMode == CompressionMode.Compress)
                    output = Compress(data.Chunk);
                else
                    output = Decompress(data.Chunk, bfs);

                //TODO: Organization right order of chunks
                //1. Блокировать тред, пока не наступит очередь его чанка

                while (data.Num != prevchunk + 1)
                {
                    Thread.Sleep(1);
                }
                
                outputQueue.Enqueue(output);
                prevchunk = data.Num;
            }
        }

        private static int ReadChunkSize(FileStream stream)
        {
            MemoryStream ms = new MemoryStream();

            while (true)
            {
                var tmp = stream.ReadByte();
                if (tmp != -1)
                {
                    if ((char)tmp != '#')
                        ms.WriteByte((byte)tmp);
                    else
                        break;
                }
                else
                    break;
            }

            if (ms.Length == 0)
                return -1;

            return int.Parse(Encoding.ASCII.GetString(ms.ToArray()));
        }

        private static IEnumerable<byte[]> ReadFileChunk(FileStream sourceStream, int maxBufferSize)
        {
            while (true)
            {
                var buffer = new byte[maxBufferSize];
                for (var bufferIndex = 0; bufferIndex < buffer.Length; bufferIndex++)
                {
                    var dataByte = sourceStream.ReadByte();
                    if (dataByte == -1) // end of file
                    {
                        if (bufferIndex > 0)
                        {
                            Array.Resize(ref buffer, bufferIndex);
                            yield return buffer;
                        }

                        yield break;
                    }

                    buffer[bufferIndex] = (byte)dataByte;
                }

                yield return buffer;
            }
        }

        public static byte[] Compress(byte[] data)
        {
            using (var output = new MemoryStream())
            using (var gzip = new GZipStream(output, CompressionMode.Compress))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
                return output.ToArray();
            }
        }

        public static byte[] Decompress(byte[] data, int buffsize)
        {
            using (var output = new MemoryStream(buffsize))
            using (var input = new MemoryStream(data))
            using (var gzip = new GZipStream(input, CompressionMode.Decompress))
            {
                List<byte[]> chs = new List<byte[]>();
                while (true)
                {
                    byte[] bf = new byte[buffsize];
                    int cnt = gzip.Read(bf, 0, buffsize);
                    if (cnt < buffsize)
                    {
                        Array.Resize(ref bf, cnt);
                        chs.Add(bf);
                        break;
                    }
                    chs.Add(bf);
                }

                return chs.SelectMany(p => p).ToArray();
            }
        }
    }
}
