﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace MegaZipV2.Collections
{
    public class ConcurrentQueue<T> : IEnumerable<T>
    {
        private Queue<T> internalQueue = new Queue<T>();

        private EventWaitHandle dequeueResetEvent = new EventWaitHandle(false, EventResetMode.AutoReset);
        private AutoResetEvent enqueueResetEvent = new AutoResetEvent(true);

        public int Size { get; }
        public bool IsFinished { get; set; } = false;

        /// <exception cref="ArgumentOutOfRangeException">Size can not be negative.</exception>
        public ConcurrentQueue(int size)
        {
            if (size < 0)
                throw new ArgumentOutOfRangeException("Size is nagetive.", nameof(size));

            if (size == 0)
                size = int.MaxValue;

            Size = size;
        }

        /// <summary>
        /// Stores item.
        /// Blocks thread when item count exceeded Size parameter.
        /// Continues when items dequeued.
        /// </summary>
        /// <exception cref="ArgumentNullException">Arugment 'item' is null.</exception>
        /// <exception cref="InvalidOperationException">Queue already finished.</exception>
        public void Enqueue(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            enqueueResetEvent.WaitOne();

            if (IsFinished)
            {
                enqueueResetEvent.Set();
                throw new InvalidOperationException("Queue already finished.");
            }

            lock (internalQueue)
            {
                internalQueue.Enqueue(item);
                if (internalQueue.Count < Size || IsFinished)
                    enqueueResetEvent.Set();
            }

            dequeueResetEvent.Set();
        }

        /// <summary>
        /// Withdraws item.
        /// Blocks thread if no items in queue.
        /// Continues on new item queued or Finished.
        /// Returns <code>null</code> when Finished and no items left.
        /// </summary>
        /// <returns>Item when queue is not finished; null otherwise.</returns>
        public T Dequeue()
        {
            while (true)
            {
                dequeueResetEvent.WaitOne();

                lock (internalQueue)
                {
                    if (IsFinished && internalQueue.Count == 0)
                    {
                        dequeueResetEvent.Set();
                        return default(T);
                    }

                    if (internalQueue.Count > 0)
                    {
                        var item = internalQueue.Dequeue();

                        if (internalQueue.Count > 0 || IsFinished)
                            dequeueResetEvent.Set();

                        enqueueResetEvent.Set();
                        return item;
                    }
                }
            }
        }

        /// <summary>
        /// Continues all blocked threads.
        /// All blocked Dequeues will return null when no items left.
        /// </summary>
        public void Finish()
        {
            IsFinished = true;
            dequeueResetEvent.Set();
            enqueueResetEvent.Set();
        }

        #region IEnumerator

        public IEnumerator<T> GetEnumerator()
        {
            return new ConcurrentQueueEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ConcurrentQueueEnumerator<T>(this);
        }

        #endregion IEnumerator
    }
}
