﻿using System.Collections;
using System.Collections.Generic;

namespace MegaZipV2.Collections
{
    public class ConcurrentQueueEnumerator<T> : IEnumerator<T>
    {
        private ConcurrentQueue<T> concurrentQueue;

        public ConcurrentQueueEnumerator(ConcurrentQueue<T> concurrentQueue)
        {
            this.concurrentQueue = concurrentQueue;
        }

        public T Current { get; private set; }
        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            Current = concurrentQueue.Dequeue();
            return Current != null;
        }

        public void Reset()
        {
            // not suported
        }

        public void Dispose()
        {
            // nothing to dispose
        }
    }
}
