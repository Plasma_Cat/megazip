﻿namespace MegaZipV2
{
    public class NumberedChunk
    {
        public int Num { get; }
        public byte[] Chunk { get; }

        public NumberedChunk(int num, byte[] chunk)
        {
            Num = num;
            Chunk = chunk;
        }
    }
}
