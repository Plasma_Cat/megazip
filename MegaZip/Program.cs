﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;


/// <summary>
/// Программа для упаковки и распаковки файлов огромных размеров
/// Автор: Борадонов Антон Сергеевич 
/// MegaZip © 2018г.
/// </summary>
namespace MegaZip
{
  /// <summary>
  /// Архивация, потоки, консольные команды - пожалуйста, не смейтесь громко над тем, что я тут понаписал.
  /// Это не самые популярные темы в моей практике, так что прошу отнестись с пониманием.
  /// </summary>
  class Program
  {
    #region Мысли - тут коротко о том, что я собираюсь сделать и как оно должно работать
    /*
    ===============================================================================================
    Задание:
    ===============================================================================================
    Написать консольную программу на C#, предназначенную для поблочного сжатия и расжатия файлов с помощью System.IO.Compression.GzipStream. 

    Для компрессии исходный файл делится на блоки одинакового размера, например, в 1 мегабайт. Каждый блок компрессится и записывается в выходной файл независимо от остальных блоков.

    Программа должна эффективно распараллеливать и синхронизировать обработку блоков  в многопроцессорной среде и уметь обрабатывать файлы, размер которых превышает объем доступной оперативной памяти. 
    В случае исключительных ситуаций необходимо проинформировать пользователя понятным сообщением, позволяющим пользователю исправить возникшую проблему, в частности если проблемы связаны с ограничениями операционной системы.
    При работе с потоками допускается использовать только стандартные классы и библиотеки из .Net 3.5 (исключая ThreadPool, BackgroundWorker, TPL). Ожидается реализация с использованием Thread-ов.
    Код программы должен соответствовать принципам ООП и ООД (читаемость, разбиение на классы и т.д.). 
    Параметры программы, имена исходного и результирующего файлов должны задаваться в командной строке следующим образом:
    GZipTest.exe compress/decompress [имя исходного файла] [имя результирующего файла]
    ===============================================================================================
    Алгоритмы:
    ===============================================================================================
    - Определяем количество ядер процессора
    - Создаем свой файловый поток для каждого ядра
    - Прочитываем из файла по 1МБ (настраивается) в каждый поток
    - Создаем потоки для архивации прочитанных кусочков, маркируем их
    - Пишим сжатые в результирующий файловый поток.
    - Синхрогнизация по смещению позиции в потоке, например
    - Вертеть в цикле, пока не EOS
    ===============================================================================================
      */

    #endregion Мысли - тут коротко о том, что я собираюсь сделать и как оно должно работать

    #region Переменные
    /// <summary>
    /// Путь входного файла
    /// </summary>
    public static string inputpath;

    /// <summary>
    /// Путь результата
    /// </summary>
    public static string outputpath;

    /// <summary>
    /// Флаг "что делаем?": -1 - ничего, 0 - cжимаем, 1 - разжимаем
    /// </summary>
    public static int flag = -1;

    /// <summary>
    /// номер пункта меню
    /// </summary>
    private static int choice = 0;

    /// <summary>
    /// номер последнего выбранного пункта меню
    /// </summary>
    private static int lastchoice = -1;

    /// <summary>
    /// флаг успешности выполнения действия
    /// </summary>
    private static int succ = -1;

    /// <summary>
    /// заголовок, который появляется в консоли после запуска программы
    /// </summary>
    static string hellow = "========================================\r\n" +
                           "===========  MegaZip © 2018  ===========\r\n" +
                           "========================================\r\n";

    /// <summary>
    /// Список команд
    /// </summary>
    static string[] commands = { "-megazip", "-megaunzip", "-help", "-about", "-creat32", "-exit" };

    /// <summary>
    /// разделитель
    /// </summary>
    static string sep = "\r\n========================================";

    /// <summary>
    /// сообщение о непредвиденной ситуации
    /// </summary>
    static string unexpectaction = "\r\nНепредусмотренное действие.";

    /// <summary>
    /// сообщение о том, что нужно нажать любую кнопку
    /// </summary>
    static string hitanykey = "\r\nНажмите любую клавишу.";

    /// <summary>
    /// текст подсказки
    /// </summary>
    static string cmdhelp = "\r\nПрограмма MegaZip © 2018 принимает следующие параметры:" +
                            "\r\n[путь_исходного_файла] [папка_сохранения_результата] [команда]" +
                            "\r\n\nСписок команд:" +
                            "\r\n-megazip - сжать исходный файл" +
                            "\r\n-megaunzip - распаковать исходный файл" +
                            "\r\n-help - показать эту справку" +
                            "\r\n-about - показать описание программы." +
                            "\r\n-creat32 - создать файл огромных размеров (для тестов)." +
                            "\r\n-exit - выйти из программы.";

    /// <summary>
    /// текст О Программе
    /// </summary>
    static string cmdabout = sep +
                             "\r\nПрограмма MegaZip © 2018" +
                             sep +
                             "\r\nАвтор: Борданов Антон Сергеевич" +
                             "\r\nТeл: 89196731392" +
                             "\r\nE-ml: ant_bord@bk.ru\n" + sep +
                             "\r\nВыполняет упаковку и распаковку файлов, размер которых может превышать объемы ОЗУ." +
                             "\r\nПрограмма была написана в рамках выполнения особого зхадания.";

    /// <summary>
    /// Размер прочиываемого куска из файлового потока в БАЙТАХ!
    /// </summary>
    public static int stepsize = 1000000;

    static string test_inpth;
    static string test_outpth;


    #endregion Переменные

    #region Менюшечка
    /// <summary>
    /// Вспомнился первый курс универа. Решил сделать менюшечку для выбора команд.
    /// </summary>
    static readonly string _cursor = "=> ";

    /// <summary>
    /// Шапка программы - пути и прочее.
    /// </summary>
    private static void Specifications()
    {
      Console.ForegroundColor = ConsoleColor.White;
      Console.WriteLine(hellow);
      Console.WriteLine("Путь входного файла: {0}", inputpath);
      Console.WriteLine("Путь выходного файла: {0}", outputpath);
      Console.WriteLine("Последнее действие: {0} - {1}", lastchoice < 0 ? "" : commands[lastchoice],
                                                         succ == 1 ? "Успешно" : (succ == 0 ? "Отказ" : " "));
      Console.WriteLine(sep);
      Console.WriteLine("\r\nВыберите действие:");
    }

    /// <summary>
    /// Рисуем меню
    /// </summary>
    /// <param name="i_choice">номер выбранной строки</param>
    private static void MenuPrint(int i_choice)
    {
      Console.Clear();
      Specifications();
      for (int i = 0; i < commands.Length; i++)
      {
        if (i == i_choice)
        {
          Console.ForegroundColor = ConsoleColor.Green;
          Console.Write(_cursor);
          Console.ForegroundColor = ConsoleColor.White;
        }
        if (i == i_choice)
        {
          Console.ForegroundColor = ConsoleColor.Green;
        }
        else Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine(commands[i]);
      }
    }

    /// <summary>
    /// Непосредственная реализация меню
    /// </summary>
    /// <returns></returns>
    private static int Menu()
    {
      while (true)
      {
        MenuPrint(choice);
        switch (Console.ReadKey().Key)
        {
          case ConsoleKey.UpArrow:
            if (choice != 0)
              --choice;
            break;
          case ConsoleKey.DownArrow:
            if (choice != commands.Length - 1)
              ++choice;
            break;
          case ConsoleKey.Enter:
            Console.CursorVisible = true;
            lastchoice = choice;
            return choice;
        }
      }
    }
    #endregion Менюшечка

    /// <summary>
    /// Спросить у пользователя путь
    /// </summary>
    /// <returns>1 - удалось получить пути, 0 - не удалось</returns>
    private static bool AskPath()
    {
#if DEBUG
      inputpath = test_inpth;
      outputpath = Path.GetFileNameWithoutExtension(inputpath);
      return true;
#endif

      int trys = 0;//кол-во попыток. Чтобы в какой-то момент прекратить бессмысленные страдания.
      int tryall = 5;//Допустим, всего можно пытаться ввести данные по столько раз.

      while (true)
      {
        Console.WriteLine("\r\nВведите путь исходного файла:");
        inputpath = Console.ReadLine().ToString();

        if (!string.IsNullOrEmpty(inputpath))
        {
          if (File.Exists(inputpath))
          {
            trys = 0;
            break;
          }
          else
          {
            Console.WriteLine("\r\nВыбранный файл не существует.\r\nПопробуйте еще раз.");
            trys++;
          }
        }
        else
        {
          Console.WriteLine("\r\nПуть исходного файла не должен быть пустым.\r\nПопробуйте еще раз.");
          trys++;
        }

        if (trys >= tryall)
        {
          Console.WriteLine("\r\nВидимо, что-то пошло совсем не так.\nДавайте оставим попытки и сходим передохнем." +
                            hitanykey);
          Console.ReadLine();
          succ = 0;
          return false;
        }
      }

      Console.WriteLine("\r\nВведите путь выходного файла (можно оставить пустым):");
      outputpath = Console.ReadLine().ToString();

      if (string.IsNullOrEmpty(outputpath))
      {
        Console.WriteLine("\r\nПоскольку путь результата введен не был, он будет выбран автоматически." +
                          "\nРезультат действия будет находиться в там же, где исходный файл.");
        var fi = new FileInfo(inputpath);

        outputpath = string.Format("{0}{1}", fi.Directory, Path.GetFileNameWithoutExtension(inputpath));
      }

      Console.WriteLine(sep + "\r\nПоехали!" + sep);
      succ = 1;
      return true;
    }

    /// <summary>
    /// Спросить путь
    /// </summary>
    /// <param name="path_">путь</param>
    /// <returns>1 - успешное, 0 - не получилось спросить путь</returns>
    private static bool AskPath32(out string path_)
    {
      int trys = 0;//кол-во попыток. Чтобы в какой-то момент прекратить бессмысленные страдания.
      int tryall = 5;//Допустим, всего можно пытаться ввести данные по столько раз.

      while (true)
      {
        Console.WriteLine("\r\nВведите путь исходного файла:");
        path_ = Console.ReadLine().ToString();

        if (!string.IsNullOrEmpty(path_))
        {
          trys = 0;
          break;
        }
        else
        {
          Console.WriteLine("\r\nПуть исходного файла не должен быть пустым.\r\nПопробуйте еще раз.");
          trys++;
        }

        if (trys >= tryall)
        {
          Console.WriteLine("\r\nВидимо, что-то пошло совсем не так.\nДавайте оставим попытки и сходим передохнем." +
                            hitanykey);
          Console.ReadLine();

          return false;
        }
      }
      return true;
    }

    /// <summary>
    /// Создать файл размером Х ГБ
    /// </summary>
    /// <param name="x_">размер создаваемого файла в ГБ</param>
    private static void Create32(double x_)
    {
      string pth = string.Empty;

      if (AskPath32(out pth))
      {
        char cr = 'q';
        ulong count = 0;
        ulong fsize = (ulong)(x_ * 1024 * 1024 * 1024);//переводим к байтам
        Random d = new Random();

        FileInfo f = new FileInfo(pth);
        var s = File.AppendText(pth);

        while (count < fsize)
        {
          cr = (char)d.Next(1, 1000);
          s.Write(cr);
          count += sizeof(char);
        }

        s.Close();
        outputpath = pth;
      }
    }

    /// <summary>
    /// Создать файл размером Х байтах
    /// </summary>
    /// <param name="x_">размер создаваемого файла в байтах</param>
    private static int Create32(ulong x_)
    {
      string pth = string.Empty;

      if (AskPath32(out pth))
      {
        byte cr = 0;
        ulong count = 0;
        ulong fsize = x_ * sizeof(byte);//переводим к байтам
        Random d = new Random();

        FileInfo f = new FileInfo(pth);
        var s = File.OpenWrite(pth);

        while (count < fsize)
        {
          //cr = (byte)d.Next(0, 255);
          cr = 33;
          s.WriteByte(cr);
          count += sizeof(byte);
        }

        s.Close();
        outputpath = pth;
        return 1;
      }
      return 0;
    }

    /// <summary>
    /// Погнали!
    /// </summary>
    /// <param name="args">Список флагов и прочих параметров (Что? Куда? Что сделать?):
    /// Сnрока в формате: "MegaZip D:/Work/MegaOne.big D:/Work/Archives/ -megazip"
    /// +file - путь к файлу, который необходимо сжать/разжать;
    /// +path - путь, куда класть результат;
    /// -megazip - флаг действия над файлом (сжать);
    /// -megaunzip - флаг действия над файлом (разжать).
    /// </param>
    static void Main(string[] args)
    {
#if DEBUG
      test_inpth = @"d:\1.dat";
#else
      test_inpth = "";
#endif

      #region Обработка аргументов
      if (args.Contains(commands[2]))
      {
        Console.WriteLine(sep + cmdhelp + hitanykey);
        Console.ReadKey();
        //return;
      }

      if (args.Contains(commands[3]))
      {
        Console.WriteLine(sep + cmdabout + hitanykey);
        Console.ReadKey();
        //return;
      }

      if (args.Contains(commands[4]))
      {
        Console.WriteLine(sep + "\r\nЗабавно! Запустить программу, чтобы выйти из нее!" + hitanykey);
        Console.ReadKey();
        return;
      }
      #endregion Обработка аргументов

      Console.Clear();
      Console.ForegroundColor = ConsoleColor.White;
      Console.WriteLine(hellow);

      //Программа в этой версии принимает РОВНО три аргумента: путь что, пусть куда и флаг "что делать". Прочие варианты являются ошибкой.
      if (args.Length == 3)
      {
        #region Консольный запуск
        inputpath = args[0];
        outputpath = args[1];
        flag = args[2].Equals(commands[0]) ? 0 : (args[2].Equals(commands[1]) ? 1 : -1);

        switch (flag)
        {
          case 0:
            {
              #region СЖИМАЕМ!
              if (string.IsNullOrEmpty(outputpath))
              {
                Console.WriteLine("\r\nПоскольку путь результата введен не был, он будет выбран автоматически." +
                                  "\nРезультат действия будет находиться в там же, где исходный файл.");
                var fi = new FileInfo(inputpath);

                outputpath = string.Format("{0}{1}", fi.Directory, Path.GetFileNameWithoutExtension(inputpath));
              }

              outputpath += ".big";
              Console.WriteLine("\r\nСжимаем сюда: {0}", outputpath);

              //тут сжимаем...

              succ = 1;//пусть будет, как будто нам это удалось              
              #endregion СЖИМАЕМ!
            }
            break;
          case 1:
            {
              #region РАЗЖИМАЕМ!
              Console.WriteLine("\r\nРазжимаем сюда: {0}{1}", outputpath);

              //тут сжимаем...

              succ = 1;//пусть будет, как будто нам это удалось             
              #endregion РАЗЖИМАЕМ!
            }
            break;
          default:
            Console.WriteLine(sep + unexpectaction);
            Console.ReadKey();
            break;
        }
        #endregion Консольный запуск
      }
      else if (args.Length < 3)//Неконсольный запуск - типа запустили без аргументов и хотим ввести все параметры вручную
      {
        #region Обычный пуск
        do
        {
          flag = Menu();

          switch (flag)
          {
            case 0:
              {
                #region СЖИМАЕМ!
                if (AskPath())
                {
                  outputpath += ".big";
                  Console.WriteLine("\r\nСжимаем сюда: {0}", outputpath);

                  //тут сжимаем...
  
                  Console.ReadKey();
                }
                #endregion СЖИМАЕМ!
              }
              break;
            case 1:
              {
                #region РАЗЖИМАЕМ!
                if (AskPath())
                {
                  //outputpath += new FileInfo(inputpath).Extension;
                  Console.WriteLine("\r\nРазжимаем сюда: {0}", outputpath);

                  //тут разжимаем...

                  Console.ReadKey();
                }
                #endregion РАЗЖИМАЕМ!
              }
              break;

            case 2:
              #region Help - показать список команд
              Console.WriteLine(cmdhelp + hitanykey);
              succ = 1;
              Console.ReadKey();
              #endregion Help - показать список команд
              break;

            case 3:
              #region About - о программе
              Console.WriteLine(cmdabout + hitanykey);
              succ = 1;
              Console.ReadKey();
              #endregion About - о программе
              break;

            case 4:
              #region Create32 - создать огромный файл для тестов
              Console.WriteLine("\r\nВведите размер в байтах: ");
              ulong sz = 0;
              if (ulong.TryParse(Console.ReadLine(), out sz))
              {
                succ = Create32(sz);
                Console.WriteLine("\r\nГотово!");
              }
              else
              {
                Console.WriteLine("\r\nНе сумел распозначть введенное число.\nПопробуйте еще раз.");
                succ = 0;
              }

              Console.WriteLine(hitanykey);
              Console.ReadKey();
              #endregion Create32 - создать огромный файл для тестов
              break;

            case 5:
              #region Exit - о программе
              succ = 1;
              return;
            #endregion Exit - о программе
            //break;

            default:
              Console.WriteLine(sep + unexpectaction + hitanykey);
              succ = 0;
              Console.ReadKey();
              break;
          }
        } while (true);
        #endregion Обычный пуск
      }
      else
      {
        Console.WriteLine(sep + "\r\nПрограмма в этой версии принимает РОВНО три аргумента: " +
                                "\n - путь что\n - путь куда\n - флаг \"что делать\"." +
                                "\r\nПроверьте правильность аркументов." + hitanykey);
        Console.ReadKey();
        return;
      }

      Console.WriteLine(sep + hitanykey + sep);
      Console.ReadLine();
    }
  }
}
